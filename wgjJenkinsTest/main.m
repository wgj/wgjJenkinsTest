//
//  main.m
//  wgjJenkinsTest
//
//  Created by wangguanjun on 2017/11/29.
//  Copyright © 2017年 DongFangRongZiWang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
